<?php

namespace App\Http\Controllers;

use App\CaoUsuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ConsultorController extends Controller
{
    //
    public function relatorioCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $consultorfacturas = DB::table('caoos')
            ->join('caosalario', 'caosalario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caofatura', 'caofatura.co_os', '=', 'caoos.co_os')
            ->where('caoos.co_usuario', $clientesSelectinados)
            ->select('caofatura.*','caosalario.*','caousuario.*','caoos.*', DB::raw('monthname(data_emissao) AS  month, sum(valor) as liquida, sum(total_imp_inc) as imposto'))
            ->groupBy(DB::raw('month'))
            ->get();

        $consultores = DB::table('permissao_sistema')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'permissao_sistema.co_usuario')
            ->join('tipo_user', 'tipo_user.co_tipo_usuario', '=', 'permissao_sistema.co_tipo_usuario')
            ->where('permissao_sistema.co_sistema', 1)
            ->where('permissao_sistema.in_ativo', 'S')
            ->whereIn('permissao_sistema.co_tipo_usuario', [1, 2, 3])
            ->select('caousuario.*')
            ->distinct()
            ->get();

        $pesquisa = 1;    

        return view('porCliente',['consultorfacturas' =>$consultorfacturas,'consultores'=>$consultores,'pesquisa'=>$pesquisa ]);

    }

    public function pizaCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $consultorfacturas = DB::table('caoos')
            ->join('caosalario', 'caosalario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caofatura', 'caofatura.co_os', '=', 'caoos.co_os')
            ->where('caoos.co_usuario', $clientesSelectinados)
            ->select('caofatura.*','caosalario.*','caousuario.*','caoos.*', DB::raw('sum(valor) as liquida, sum(total_imp_inc) as imposto'))
            ->distinct()
            ->get();

        $label=[];
        $valor=[];

        foreach ($consultorfacturas as $clientesfactura) {
             $label[] = $clientesfactura->no_usuario;
             $valor[] = $clientesfactura->liquida*($clientesfactura->imposto)/100;
        }

        //return   $clientesSelectinados;   
        $consultores = DB::table('permissao_sistema')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'permissao_sistema.co_usuario')
            ->join('tipo_user', 'tipo_user.co_tipo_usuario', '=', 'permissao_sistema.co_tipo_usuario')
            ->where('permissao_sistema.co_sistema', 1)
            ->where('permissao_sistema.in_ativo', 'S')
            ->whereIn('permissao_sistema.co_tipo_usuario', [1, 2, 3])
            ->select('caousuario.*')
            ->distinct()
            ->get();

        $pesquisa = 2;    

        return view('porCliente',['label' =>$label,'valor'=>$valor,'consultores'=>$consultores,'pesquisa'=>$pesquisa ]);

    }

    public function graficoCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $consultorfacturas = DB::table('caoos')
            ->join('caosalario', 'caosalario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'caoos.co_usuario')
            ->join('caofatura', 'caofatura.co_os', '=', 'caoos.co_os')
            ->where('caoos.co_usuario', $clientesSelectinados)
            ->select('caofatura.*','caosalario.*','caousuario.*','caoos.*', DB::raw('sum(valor) as liquida, sum(total_imp_inc) as imposto'))
            ->distinct()
            ->get();

        $label=[];
        $valor=[];

        foreach ($consultorfacturas as $clientesfactura) {
             $label[] = $clientesfactura->no_usuario;
             $valor[] = $clientesfactura->liquida*($clientesfactura->imposto)/100;
        }

        //return   $clientesSelectinados;   
        $consultores = DB::table('permissao_sistema')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'permissao_sistema.co_usuario')
            ->join('tipo_user', 'tipo_user.co_tipo_usuario', '=', 'permissao_sistema.co_tipo_usuario')
            ->where('permissao_sistema.co_sistema', 1)
            ->where('permissao_sistema.in_ativo', 'S')
            ->whereIn('permissao_sistema.co_tipo_usuario', [1, 2, 3])
            ->select('caousuario.*')
            ->distinct()
            ->get();

        $pesquisa = 3;    

        return view('porCliente',['label' =>$label,'valor'=>$valor,'consultores'=>$consultores,'pesquisa'=>$pesquisa ]);

    }


}
