<?php

namespace App\Http\Controllers;

use App\CaoCliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ClienteController extends Controller
{
    //
    public function porConsultor()
    {
        $clientes =  CaoCliente::where('tp_cliente','A')->get();

        $pesquisa = 0;

        return view('welcome',['clientes' =>$clientes,'pesquisa'=>$pesquisa ]);
    }

    public function porCliente()
    {
        $consultores = DB::table('permissao_sistema')
            ->join('caousuario', 'caousuario.co_usuario', '=', 'permissao_sistema.co_usuario')
            ->join('tipo_user', 'tipo_user.co_tipo_usuario', '=', 'permissao_sistema.co_tipo_usuario')
            ->where('permissao_sistema.co_sistema', 1)
            ->where('permissao_sistema.in_ativo', 'S')
            ->whereIn('permissao_sistema.co_tipo_usuario', [1, 2, 3])
            ->select('caousuario.*')
            ->get();

            $pesquisa = 0;    

        return view('porCliente',['consultores' =>$consultores,'pesquisa'=>$pesquisa]);

    }

    public function relatorioCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $clientesfacturas = DB::table('caofatura')
            ->join('caocliente', 'caocliente.co_cliente', '=', 'caofatura.co_cliente')
            ->whereIn('caocliente.co_cliente', $clientesSelectinados)
            ->select('caofatura.*','caocliente.*', DB::raw('monthname(data_emissao) AS  month, sum(valor) as valor'))
            ->groupBy(DB::raw('month'))
            ->get();

        $clientes =  CaoCliente::where('tp_cliente','A')->get();

        $pesquisa = 1;    

        return view('welcome',['clientesfacturas' =>$clientesfacturas,'clientes'=>$clientes,'pesquisa'=>$pesquisa ]);

    }

    public function pizaCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $clientesfacturas = DB::table('caofatura')
            ->join('caocliente', 'caocliente.co_cliente', '=', 'caofatura.co_cliente')
            ->whereIn('caocliente.co_cliente', $clientesSelectinados)
            ->select('caofatura.*','caocliente.*', DB::raw('sum(valor) as valor'))
            ->groupBy(DB::raw('caocliente.no_razao'))
            ->get();

        $label=[];
        $valor=[];

        foreach ($clientesfacturas as $clientesfactura) {
             $label[] = $clientesfactura->no_razao;
             $valor[] = $clientesfactura->valor*($clientesfactura->total_imp_inc)/100;
        }

        //return   $clientesSelectinados;   

        $clientes =  CaoCliente::where('tp_cliente','A')->get();

        $pesquisa = 2;    

        return view('welcome',['label' =>$label,'valor'=>$valor,'clientes'=>$clientes,'pesquisa'=>$pesquisa ]);

    }

    public function graficoCliente(Request $request)
    {
        $clientesSelectinados = $request->get('duallistbox_demo1');

        $clientesfacturas = DB::table('caofatura')
            ->join('caocliente', 'caocliente.co_cliente', '=', 'caofatura.co_cliente')
            ->whereIn('caocliente.co_cliente', $clientesSelectinados)
            ->select('caofatura.*','caocliente.*', DB::raw('sum(valor) as valor'))
            ->groupBy(DB::raw('caocliente.no_razao'))
            ->get();

        $clientes =  CaoCliente::where('tp_cliente','A')->get(); 

        $clientesSelect =  CaoCliente::whereIn('co_cliente',$clientesSelectinados)->get();      

        $label=[];
        $valor=[];
        $total = [];

        foreach ($clientesfacturas as $clientesfactura) {
             $label[] = $clientesfactura->no_razao;
             $valor[] = $clientesfactura->valor*($clientesfactura->total_imp_inc)/100;
             $total[] = $clientesfactura->total;
        }




        $pesquisa = 3;    

        return view('welcome',['total'=>$total,'label' =>$label,'valor'=>$valor,'clientes'=>$clientes,'pesquisa'=>$pesquisa ]);

    }


}
