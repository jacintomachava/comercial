<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePermissaoSistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permissao_sistema', function (Blueprint $table) {
            $table->string('co_usuario',30);
            $table->bigInteger('co_tipo_usuario');
            $table->bigInteger('co_sistema');
            $table->char('in_ativo',10);
            $table->string('co_usuario_atualizacao',30);
            $table->dateTime('dt_atualizacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permissao_sistema');
    }
}
