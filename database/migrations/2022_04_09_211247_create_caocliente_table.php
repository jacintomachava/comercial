<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaoclienteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caocliente', function (Blueprint $table) {
            $table->bigInteger('co_cliente');
            $table->string('no_razao',50);
            $table->string('no_fantasia',50);
            $table->string('no_contato',30);
            $table->string('nu_telefone',20);
            $table->string('nu_ramal',6);
            $table->string('nu_cnpj',18);
            $table->string('ds_endereco',150);
            $table->integer('nu_numero');
            $table->string('ds_complemento',150);
            $table->string('no_bairro',50);
            $table->string('nu_cep',10);
            $table->string('no_pais',50);
            $table->bigInteger('co_ramo');
            $table->bigInteger('co_cidade');
            $table->integer('co_status');
            $table->string('ds_site',50);
            $table->string('ds_email',50);
            $table->string('ds_cargo_contato',50);
            $table->char('tp_cliente',2);
            $table->string('ds_referencia',100);
            $table->integer('co_complemento_status');
            $table->string('nu_fax',15);
            $table->string('ddd2',10);
            $table->string('telefone2',20);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caocliente');
    }
}
