<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaosistemaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caosistema', function (Blueprint $table) {
            $table->bigInteger('co_sistema');
            $table->bigInteger('co_cliente');
            $table->string('co_usuario',50);
            $table->integer('co_arquitetura');
            $table->string('no_sistema');
            $table->text('ds_sistema_resumo');
            $table->text('ds_caracteristica');
            $table->text('ds_requisito');
            $table->string('no_diretoria_solic',100);
            $table->string('ddd_telefone_solic',5);
            $table->string('nu_telefone_solic',20);
            $table->string('no_usuario_solic',20);
            $table->date('dt_solicitacao');
            $table->date('dt_entrega');
            $table->integer('co_email');

            $table->dateTime('dt_atualizacao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caosistema');
    }
}
