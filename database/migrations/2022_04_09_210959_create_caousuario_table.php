<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaousuarioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caousuario', function (Blueprint $table) {
            $table->string('co_usuario',30);
            $table->string('no_usuario',50);
            $table->string('ds_senha',14);
            $table->string('co_usuario_autorizacao',20);
            $table->date('dt_nascimento');
            $table->date('dt_admissao_empresa');
            $table->date('dt_desligamento');
            $table->date('dt_inclusao');
            $table->date('dt_expiracao');
            $table->string('nu_cpf',14);
            $table->string('nu_rg',20);
            $table->string('no_orgao_emissor',20);
            $table->string('uf_orgao_emissor',2);
            $table->string('ds_endereco',142);
            $table->string('no_email',100);
            $table->string('no_email_pessoal',100);
            $table->string('nu_telefone',64);
            $table->date('dt_alteracao');
            $table->string('url_foto',255);
            $table->string('instant_messenger',80);
            $table->integer('icq');
            $table->string('msn',50);
            $table->string('yms',50);
            $table->string('ds_comp_end',50);
            $table->string('ds_bairro',30);
            $table->string('nu_cep',10);
            $table->string('no_cidade',50);
            $table->string('uf_cidade',2);
            $table->date('dt_expedicao',2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caousuario');
    }
}
