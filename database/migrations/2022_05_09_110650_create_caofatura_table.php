<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaofaturaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caofatura', function (Blueprint $table) {
            $table->bigInteger('co_fatura');
            $table->bigInteger('co_cliente');
            $table->bigInteger('co_sistema');
            $table->bigInteger('co_os');
            $table->bigInteger('num_nf');
            $table->float('total');
            $table->float('valor');
            $table->date('data_emissao');
            $table->text('corpo_nf');
            $table->float('comissao_cn');
            $table->float('total_imp_inc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caofatura');
    }
}
