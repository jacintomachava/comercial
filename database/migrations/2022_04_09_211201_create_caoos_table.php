<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCaoosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caoos', function (Blueprint $table) {
            $table->bigInteger('co_os');
            $table->bigInteger('nu_os');
            $table->integer('co_sistema');
            $table->string('co_usuario',50);
            $table->integer('co_arquitetura');
            $table->string('ds_os',200);
            $table->string('ds_caracteristica',200);
            $table->string('ds_requisito',200);
            $table->date('dt_inicio');
            $table->date('dt_fim');
            $table->integer('co_status');
            $table->string('diretoria_sol',50);
            $table->date('dt_sol');
            $table->string('nu_tel_sol',20);
            $table->string('ddd_tel_sol',5);
            $table->string('nu_tel_sol2',20);
            $table->string('ddd_tel_sol2',5);
            $table->string('usuario_sol',50);
            $table->date('dt_imp');
            $table->date('dt_garantia');
            $table->integer('co_email');
            $table->integer('co_os_prospect_rel');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caoos');
    }
}
