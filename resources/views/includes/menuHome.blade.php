<nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
        <div class="sidebar-brand">
            <a href="#">Comercial</a>
            <div id="close-sidebar">
                <i class="fas fa-list"></i>
            </div>
        </div>
        <div class="sidebar-header">
            <div class="user-pic">
                <img class="img-responsive img-rounded" src="{{URL::to('public/img/logo.gif')}}" height="500" width="500" alt="User picture">
            </div>
            <div class="user-info">
                <span class="user-role">Administrator</span>
                <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
            </div>
        </div>

        <div class="sidebar-menu">
            <ul>

                <li class="sidebar-dropdown">
                    <a>
                        <i class="fa fa-user"></i>
                        <span>Comercial</span>
                    </a>
                    <div class="sidebar-submenu">
                        <ul>
                            <li>
                                <a  href="{{route('por.cliente')}}">Por Consultor
                                </a>
                            </li>
                            <li>
                                <a  href="{{route('por.consultor')}}">Por Cliente
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>



            </ul>
        </div>
        <!-- sidebar-menu  -->
    </div>


    <!-- sidebar-content  -->
    <div class="sidebar-footer">
        <a href="#">
            <i class="fa fa-bell"></i>
            <span class="badge badge-pill badge-warning notification">3</span>
        </a>
        <a href="#">
            <i class="fa fa-envelope"></i>
            <span class="badge badge-pill badge-success notification">7</span>
        </a>
        <a href="#">
            <i class="fa fa-cog"></i>
            <span class="badge-sonar"></span>
        </a>
        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" >
            <i class="fa fa-power-off"></i>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>


</nav>