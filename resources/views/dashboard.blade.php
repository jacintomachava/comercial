
{{-- Chamar layout --}}
@extends('layouts.masterHome')

{{-- Chamar css --}}
@section('css')

 

@endsection

{{-- Chamar conteudo --}}
@section('conteudo')


    <!-- sidebar-wrapper  -->



        <div class="row">
            
             <div class="col-5">
              <canvas id="myChart1" height="200%"></canvas>
            </div>
            <div class="col-7">
               <canvas id="myChart" ></canvas>
            </div>
        </div>

        <hr style="background: #60a18a; color: blue">

        <div class="row">
            
             <div class="col-5">
              <canvas id="myChartMod" height="200%"></canvas>
            </div>

            <div class="col-7">
               <canvas id="myChartMes" ></canvas>
            </div>
        </div>

         <hr style="background: #60a18a; color: blue">




    <!-- page-content" -->

@endsection



{{-- Chamar js --}}
@section('js')

    <script>

    var xValues = {!! json_encode($catlabel) !!};
    var yValues =  {!! json_encode($catnumero) !!};

    var barColors = ["red", "green","blue","orange","brown"];

    new Chart("myChart", {
      type: "bar",
      data: {
        labels: xValues,
        datasets: [{
          backgroundColor: barColors,
          data: yValues
        }]
      },
      options: {
        legend: {display: false},
        title: {
          display: true,
          text: "Trabalho a Apresentar"
        }
      }
    });
    </script>

   <script>

    var xValues = {!! json_encode($labelmes) !!};
    var yValues =  {!! json_encode($numemes) !!};

    var barColors = ["red", "green","blue","orange","brown"];

    new Chart("myChartMes", {
      type: "bar",
      data: {
        labels: xValues,
        datasets: [{
          backgroundColor: barColors,
          data: yValues
        }]
      },
      options: {
        legend: {display: false},
        title: {
          display: true,
          text: "Inscritos por Mes"
        }
      }
    });
    </script>

     <script>
        var xValues = {!! json_encode($labelmod) !!};
        var yValues =  {!! json_encode($numemod) !!};
        var barColors = [
          "#2b5797",
          "#e8c3b9",
          "#1e7145"
        ];

        new Chart("myChartMod", {
          type: "pie",
          data: {
            labels: xValues,
            datasets: [{
              backgroundColor: barColors,
              data: yValues
            }]
          },
          options: {
            title: {
              display: true
            }
          }
        });
    </script>


    <script>
        var xValues = {!! json_encode($labelpag) !!};
        var yValues =  {!! json_encode($numepag) !!};
        var barColors = [
          "#4cb100",
          "#fd0700",
          "#2b5797",
          "#e8c3b9",
          "#1e7145"
        ];

        new Chart("myChart1", {
          type: "pie",
          data: {
            labels: xValues,
            datasets: [{
              backgroundColor: barColors,
              data: yValues
            }]
          },
          options: {
            title: {
              display: true
            }
          }
        });
    </script>



@endsection