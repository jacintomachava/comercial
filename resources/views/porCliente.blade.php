
{{-- Chamar layout --}}
@extends('layouts.masterHome')


{{-- Chamar css --}}
@section('css')


@endsection



{{-- Chamar conteudo --}}
@section('conteudo')

    <!-- Demo -->
    <div class="container">
        <div class="row" >
            <div class="col">
                <h3> Por Consultor </h3>

            </div>
        </div>
        <div class="row" style="margin-bottom: 40px;">
            <div class="col">
                <form id="demoform">

                    {{ csrf_field() }}

                    <select multiple="multiple" size="10" name="duallistbox_demo1[]" title="duallistbox_demo1[]">
                      @foreach($consultores as $consultor)
                        <option value="{{$consultor->co_usuario}}">{{$consultor->no_usuario}}</option>
                      @endforeach
                    </select>
                    <br>
                    <div class="row">
                        <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('consultor.relatorio')}}" formmethod="post" class="btn btn-primary w-100">Relatorio</button>
                        </div>
                         <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('piza.consultor')}}" formmethod="post"  class="btn btn-primary w-100">Pizza</button>
                        </div>
                         <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('grafico.consultor')}}" formmethod="post" class="btn btn-primary w-100">Grafico</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>

       @if($pesquisa==1)


        <table id="example" class="table table-striped table-bordered responsivo" >
                <thead>
                            <tr>
                                <th>Periodo</th>
                                <th>Consultor</th>
                                <th>Receita Liquida</th>
                                <th>Custo Fixo</th>
                                <th>Comissão</th>
                                <th>Lucro</th>
                                <th>Total</th>
     
                            </tr>
                            </thead>
                <tbody>

                           @foreach($consultorfacturas as $consultor)

                                <tr>
                                    <td>{{$consultor->month}}</td>
                                    <td>{{$consultor->no_usuario}}</td>
                                    <td>{{$consultor->liquida - $consultor->liquida*($consultor->imposto)/100}}</td>
                                    <td>{{$consultor->brut_salario}}</td>
                                    <td>{{$consultor->liquida*$consultor->imposto/100*$consultor->comissao_cn/100}}</td>
                                    <td>{{$consultor->liquida-($consultor->brut_salario+$consultor->liquida*$consultor->imposto/100*$consultor->comissao_cn/100)}}</td>
                                    <td>{{$consultor->brut_salario}}</td>

                                </tr>

                           @endforeach

                            </tbody>
        </table>

       @endif

        @if($pesquisa==2)
           <div class="row">
            
                <div class="col-12">
                  <canvas id="myChartMod" height="100%" ></canvas>
                </div>

           </div>
        @endif

        @if($pesquisa==3)
            <div class="row">
            
                <div class="col-12">
                 <canvas id="myChart" height="100%" ></canvas>
                 </div>

            </div>
        @endif




@endsection



{{-- Chamar js --}}
@section('js')

    <script>
        var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Move all',
            removeAllLabel: 'Remove all'
        });

    </script>

        @if($pesquisa==2)

         <script>
        var xValues = {!! json_encode($label) !!};
        var yValues =  {!! json_encode($valor) !!};
        var barColors = ["red", "green","blue","orange","brown"];

        new Chart("myChartMod", {
          type: "pie",
          data: {
            labels: xValues,
            datasets: [{
              backgroundColor: barColors,
              data: yValues
            }]
          },
          options: {
            title: {
              display: true
            }
          }
        });
    </script>

    @endif

    @if($pesquisa==3)

         <script>

   var xValues = {!! json_encode($label) !!};
   var yValues =  {!! json_encode($valor) !!};

    var barColors = ["red", "green","blue","orange","brown"];

    new Chart("myChart", {
      type: "bar",
      data: {
        labels: xValues,
        datasets: [{
          backgroundColor: barColors,
          data: yValues
        }]
      },
      options: {
        legend: {display: false},
        title: {
          display: true,
          text: "Performance Comercial"
        }
      }
    });
    </script>

    @endif


@endsection