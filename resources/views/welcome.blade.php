
{{-- Chamar layout --}}
@extends('layouts.masterHome')


{{-- Chamar css --}}
@section('css')


@endsection



{{-- Chamar conteudo --}}
@section('conteudo')


    <!-- Demo -->
    <div class="container">
        <div class="row" >
            <div class="col">
                <h3> Por Clientes </h3>

            </div>
        </div>
        <div class="row" style="margin-bottom: 40px;">
            <div class="col">
                <form id="demoform" action="" method="post">

                    {{ csrf_field() }}

                    <select multiple="multiple" size="10" name="duallistbox_demo1[]" title="duallistbox_demo1[]">
                        @foreach($clientes as $cliente)
                            <option value="{{$cliente->co_cliente}}">{{$cliente->no_razao}}</option>
                        @endforeach
                    </select>
                    <br>
                    <div class="row">
                        <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('relatorio.cliente')}}" class="btn btn-primary w-100">Relatorio</button>
                        </div>
                         <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('piza.cliente')}}" formmethod="post"  class="btn btn-primary w-100">Pizza</button>
                        </div>
                         <div class="col-md-2 offset-md-2">
                            <button type="submit" formaction="{{route('grafico.cliente')}}" formmethod="post" class="btn btn-primary w-100">Grafico</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>


   @if($pesquisa==1)


    <table id="example" class="table table-striped table-bordered responsivo" >
            <thead>
                        <tr>
                            <th>Periodo</th>
                            <th>Cliente</th>
                            <th>Total</th>
 
                        </tr>
                        </thead>
            <tbody>

                       @foreach($clientesfacturas as $cliente)

                            <tr>
                                <td>{{$cliente->month}}</td>
                                <td>{{$cliente->no_razao}}</td>
                                 <td>{{$cliente->valor - $cliente->valor*($cliente->total_imp_inc)/100}}</td>

                            </tr>

                       @endforeach

                        </tbody>
    </table>

   @endif

   @if($pesquisa==2)
    <div class="row">
            
             <div class="col-12">
              <canvas id="myChartMod" height="100%" ></canvas>
            </div>

        </div>
   @endif

    @if($pesquisa==3)
    <div class="row">
            
             <div class="col-12">
              <canvas id="myChartMod2" height="100%" ></canvas>
            </div>

        </div>
   @endif
               

@endsection



{{-- Chamar js --}}
@section('js')

    <script>
        var demo1 = $('select[name="duallistbox_demo1[]"]').bootstrapDualListbox({
            preserveSelectionOnMove: 'moved',
            moveAllLabel: 'Move all',
            removeAllLabel: 'Remove all'
        });
    </script>



    @if($pesquisa==2)

         <script>
        var xValues = {!! json_encode($label) !!};
        var yValues =  {!! json_encode($valor) !!};
        var barColors = ["red", "green","blue","orange","brown"];

        new Chart("myChartMod", {
          type: "pie",
          data: {
            labels: xValues,
            datasets: [{
              backgroundColor: barColors,
              data: yValues
            }]
          },
          options: {
            title: {
              display: true
            }
          }
        });
    </script>

    @endif

     @if($pesquisa==3)

         <script>
        var xValues = {!! json_encode($label) !!};
        var yValues =  {!! json_encode($valor) !!};
        var tValues =  {!! json_encode($total) !!};
        var barColors = ["red", "green","blue","orange","brown"];

        new Chart("myChartMod2", {
          type: "line",
          data: {
            labels: xValues,
            datasets: [{ 
                  data: yValues,
                  borderColor: "red",
                  fill: false
                }, { 
                  data: [1600,1700,1700,1900,2000,2700,4000,5000,6000,7000],
                  borderColor: "green",
                  fill: false
                }, { 
                  data:  tValues,
                  borderColor: "blue",
                  fill: false
                }, ]
          },
          options: {
            title: {
              display: true
            }
          }
        });
    </script>

    @endif


        


@endsection