<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Disciplina
Route::post('registar/disciplina', 'ApiDisciplinaController@cadastrarDisciplina');
Route::get('listar/disciplina', 'ApiDisciplinaController@listarDisciplina');
Route::put('actualizar/disciplina/{id}', 'ApiDisciplinaController@actualizarDisciplina');
Route::delete('apagar/disciplina/{id}', 'ApiDisciplinaController@apagarDisciplina');

//Materia
Route::post('registar/materia', 'ApiMateriaController@cadastrarMateria');
Route::get('listar/materia', 'ApiMateriaController@listarMateria');
Route::put('actualizar/materia/{id}', 'ApiMateriaController@actualizarMateria');
Route::delete('apagar/materia/{id}', 'ApiMateriaController@apagarMateria');

//Exame
Route::post('registar/exame', 'ApiExameController@cadastrarExame');
Route::get('listar/exame', 'ApiExameController@listarExame');
Route::put('actualizar/exame/{id}', 'ApiExameController@actualizarExame');
Route::delete('apagar/exame/{id}', 'ApiExameController@apagarExame');


//Questao
Route::post('registar/questao', 'ApiQuestaoController@cadastrarQuestao');
Route::get('listar/questao', 'ApiQuestaoController@listarQuestao');
Route::put('actualizar/questao/{id}', 'ApiQuestaoController@actualizarQuestao');
Route::delete('apagar/questao/{id}', 'ApiQuestaoController@apagarQuestao');

//Resposta
Route::post('registar/resposta', 'ApiRespostaController@cadastrarResposta');
Route::get('listar/resposta', 'ApiRespostaController@listarResposta');
Route::put('actualizar/resposta/{id}', 'ApiRespostaController@actualizarResposta');
Route::delete('apagar/resposta/{id}', 'ApiRespostaController@apagarResposta');

//Simulador
Route::post('registar/simulador', 'ApiSimuladorController@cadastrarSimulador');
Route::get('listar/simulador', 'ApiSimuladorController@listarSimulador');
Route::put('actualizar/simulador/{id}', 'ApiSimuladorController@actualizarSimulador');
Route::delete('apagar/simulador/{id}', 'ApiSimuladorController@apagarSimulador');

//Utilizador
Route::post('registar/utilizador', 'ApiDisciplinaController@cadastrarUtilizador');
Route::get('listar/utilizador', 'ApiDisciplinaController@listarUtilizador');



