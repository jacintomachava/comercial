<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [
    'uses'=>'ClienteController@porConsultor',
    'as'=>'por.consultor'
]);

Route::get('/por/consultar', [
    'uses'=>'ClienteController@porCliente',
    'as'=>'por.cliente'
]);

Route::post('/relatorio/consultar', [
    'uses'=>'ClienteController@relatorioCliente',
    'as'=>'relatorio.cliente'
]);

Route::post('/piza/consultar', [
    'uses'=>'ClienteController@pizaCliente',
    'as'=>'piza.cliente'
]);

Route::post('/consultar/pizza', [
    'uses'=>'ConsultorController@pizaCliente',
    'as'=>'piza.consultor'
]);

Route::post('/consultar/grafico', [
    'uses'=>'ConsultorController@graficoCliente',
    'as'=>'grafico.consultor'
]);

Route::post('/grafico/cliente', [
    'uses'=>'ClienteController@graficoCliente',
    'as'=>'grafico.cliente'
]);

Route::post('/consultor/relatorio', [
    'uses'=>'ConsultorController@relatorioCliente',
    'as'=>'consultor.relatorio'
]);

Route::post('/relatorio/cliente', [
    'uses'=>'ConsultorController@relatorioConsultor',
    'as'=>'relatorio.consultor'
]);


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
